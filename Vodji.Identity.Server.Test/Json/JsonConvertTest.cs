using Vodji.Identity.Server.Auth.OAuth2.v2.Request;
using Vodji.Identity.Server.Extensions;
using Vodji.Identity.Server.Json;

namespace Vodji.Identity.Server.Test.Json
{
    public class JsonConvertTest
    {
        [Test]
        public async Task SerializeAsync()
        {
            var customJsonSerializer = new JsonConvert();
            {
                var tokenRequest = new TokenRequest
                {
                    GrantType = "client_credentials"
                };

                var jsonData = await customJsonSerializer.SerializeAsync(tokenRequest);
                {
                    Assert.That(jsonData, Does.Contain("grant_type"));
                }

                Console.WriteLine(jsonData);
            }
        }

        [Test]
        public async Task DeserializeAsync()
        {
            var customJsonSerializer = new JsonConvert();
            {
                var jsonData = "{ \"grant_type\": \"client_credentials\" }";
                var jsonResult = await customJsonSerializer.DeserializeAsync<TokenRequest>(jsonData);
                {
                    Assert.That(jsonResult.GrantType, Does.Match("client_credentials"));
                }

                Console.WriteLine($"'grant_type': {jsonResult.GrantType}");
            }
        }
    }
}