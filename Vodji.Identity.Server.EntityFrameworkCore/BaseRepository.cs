﻿using Microsoft.EntityFrameworkCore;

namespace Vodji.Identity.Server.EntityFrameworkCore
{
    public abstract class BaseRepository<TContext> : IDisposable
        where TContext : DbContext
    {

        public TContext Context { get; }
        public bool AutoSaveChanges { get; set; } = true;

        protected BaseRepository(TContext context)
        {
            Context = context;
        }

        protected Task SaveChanges(CancellationToken cancellationToken)
        {
            if (!AutoSaveChanges)
            {
                return Task.CompletedTask;
            }

            return Context.SaveChangesAsync(cancellationToken);
        }

        protected void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        public void Dispose()
        {
            _disposed = true;
            GC.SuppressFinalize(this);
        }

        private bool _disposed;
    }
}
