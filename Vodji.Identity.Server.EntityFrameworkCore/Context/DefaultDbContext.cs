﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Vodji.Identity.Server.EntityFrameworkCore.Context.Interfaces;
using Vodji.Identity.Server.EntityFrameworkCore.Roles;
using Vodji.Identity.Server.EntityFrameworkCore.Users;

namespace Vodji.Identity.Server.EntityFrameworkCore.Context
{
    public class DefaultDbContext : DefaultDbContext<User>
    {
        public DefaultDbContext(IDbConfiguration options) : base(options)
        {
        }
    }

    public class DefaultDbContext<TUser> : DefaultDbContext<TUser, Role, Guid>
        where TUser : User
    {
        public DefaultDbContext(IDbConfiguration options) : base(options)
        {
        }
    }

    public class DefaultDbContext<TUser, TRole, TKey> : DefaultDbContext<TUser, TRole, TKey, UserClaim<TKey>, UserRole<TKey>, UserLogin<TKey>, RoleClaim<TKey>, UserToken<TKey>>
        where TUser : User<TKey>
        where TRole : Role<TKey>
        where TKey : IEquatable<TKey>
    {
        public DefaultDbContext(IDbConfiguration options) : base(options)
        {
        }
    }

    public class DefaultDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken>
        : IdentityDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken>
        where TUser : User<TKey>
        where TRole : Role<TKey>
        where TKey : IEquatable<TKey>
        where TUserClaim : UserClaim<TKey>
        where TUserRole : UserRole<TKey>
        where TUserLogin : UserLogin<TKey>
        where TRoleClaim : RoleClaim<TKey>
        where TUserToken : UserToken<TKey>
    {
        public DefaultDbContext(IDbConfiguration options) : base(options.GetContextOptions())
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TRoleClaim>().ToTable("RoleClaims");
            builder.Entity<TUserToken>().ToTable("UserTokens");
            builder.Entity<TUserClaim>().ToTable("UserClaims");
            builder.Entity<TUserLogin>().ToTable("UserLogins");
            builder.Entity<TUserRole>().ToTable("UserRoles");
            builder.Entity<TRole>().ToTable("Roles");
            builder.Entity<TUser>().ToTable("Users");
        }
    }
}
