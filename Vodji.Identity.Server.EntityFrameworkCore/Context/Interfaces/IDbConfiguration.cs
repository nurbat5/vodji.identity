﻿using Microsoft.EntityFrameworkCore;

namespace Vodji.Identity.Server.EntityFrameworkCore.Context.Interfaces
{
    public interface IDbConfiguration
    {
        public DbContextOptions GetContextOptions();
    }
}
