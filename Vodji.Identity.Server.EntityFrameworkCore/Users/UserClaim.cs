﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserClaim : UserClaim<Guid> { }
    public class UserClaim<TKey> : IdentityUserClaim<TKey> where TKey : IEquatable<TKey>
    {

    }
}
