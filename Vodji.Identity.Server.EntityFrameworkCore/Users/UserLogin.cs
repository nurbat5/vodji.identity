﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserLogin : UserLogin<Guid> { }

    public class UserLogin<TKey> : IdentityUserLogin<TKey> where TKey : IEquatable<TKey>
    {
    }
}
