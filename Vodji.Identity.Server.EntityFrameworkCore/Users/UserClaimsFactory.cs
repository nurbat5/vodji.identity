﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Vodji.Identity.Server.EntityFrameworkCore.Roles;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserClaimsFactory : UserClaimsPrincipalFactory<User, Role>
    {
        public UserClaimsFactory(UserManager<User> userManager, RoleManager<Role> roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
        }
    }

    public class UserClaimsFactory<TUser> : UserClaimsPrincipalFactory<TUser, Role>
        where TUser : User<Guid>
    {
        public UserClaimsFactory(UserManager<TUser> userManager, RoleManager<Role> roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
        }
    }

    public class UserClaimsFactory<TUser, TRole> : UserClaimsPrincipalFactory<TUser, TRole>
        where TUser : User<Guid>
        where TRole : Role<Guid>
    {
        public UserClaimsFactory(UserManager<TUser> userManager, RoleManager<TRole> roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
        }
    }

    public class UserClaimsFactory<TUser, TRole, TKey> : UserClaimsPrincipalFactory<TUser, TRole>
        where TUser : User<TKey>
        where TRole : Role<TKey>
        where TKey : IEquatable<TKey>
    {
        public UserClaimsFactory(UserManager<TUser> userManager, RoleManager<TRole> roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
        }
    }
}
