﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserRole : UserRole<Guid> { }
    public class UserRole<TKey> : IdentityUserRole<TKey> where TKey : IEquatable<TKey>
    {

    }
}
