﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserPasswordValidator : UserPasswordValidator<User> 
    { 
    }
    
    public class UserPasswordValidator<TUser> : UserPasswordValidator<TUser, Guid> where TUser : User
    {
    }

    public class UserPasswordValidator<TUser, TKey> : PasswordValidator<TUser> 
        where TUser : User<TKey>
        where TKey : IEquatable<TKey>
    {

    }
}
