﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserPasswordHasher : UserPasswordHasher<User, Guid>
    {

    }

    public class UserPasswordHasher<TUser> : UserPasswordHasher<TUser, Guid>
           where TUser : User
    {

    }

    public class UserPasswordHasher<TUser, TKey> : PasswordHasher<TUser>
        where TUser : User<TKey>
        where TKey : IEquatable<TKey>
    {

    }
}
