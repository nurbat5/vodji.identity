﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserSignIn : UserSignIn<User>
    {
        public UserSignIn(UserManager<User> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<User> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<UserSignIn<User, Guid>> logger, IAuthenticationSchemeProvider schemes, IUserConfirmation<User> confirmation, RevalidatingServerAuthenticationStateProvider authenticationStateProvider) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation, authenticationStateProvider)
        {
        }
    }


    public class UserSignIn<TUser> : UserSignIn<TUser, Guid>
        where TUser : User<Guid>
    {
        public UserSignIn(UserManager<TUser> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<TUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<UserSignIn<TUser, Guid>> logger, IAuthenticationSchemeProvider schemes, IUserConfirmation<TUser> confirmation, RevalidatingServerAuthenticationStateProvider authenticationStateProvider) : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes, confirmation, authenticationStateProvider)
        {
        }
    }

    public class UserSignIn<TUser, TKey> : SignInManager<TUser>
        where TUser : User<TKey>
        where TKey : IEquatable<TKey>
    {
        public UserSignIn(
            UserManager<TUser> userManager,
            IHttpContextAccessor contextAccessor,
            IUserClaimsPrincipalFactory<TUser> claimsFactory,
            IOptions<IdentityOptions> optionsAccessor,
            ILogger<UserSignIn<TUser, TKey>> logger,
            IAuthenticationSchemeProvider schemes,
            IUserConfirmation<TUser> confirmation,
            AuthenticationStateProvider authenticationStateProvider) : base(userManager,
                                                                    contextAccessor,
                                                                    claimsFactory,
                                                                    optionsAccessor,
                                                                    logger,
                                                                    schemes,
                                                                    confirmation)
        {
            _authenticationStateProvider = authenticationStateProvider;
        }

        public async Task<ClaimsPrincipal> GetUserAsync()
        {
            var authenticationState = await _authenticationStateProvider.GetAuthenticationStateAsync();
            return authenticationState.User;
        }

        private readonly AuthenticationStateProvider _authenticationStateProvider;
    }
}
