﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserControl : UserControl<User>
    {
        public UserControl(IUserStore<User> store,
                               IOptions<IdentityOptions> optionsAccessor,
                               IPasswordHasher<User> passwordHasher,
                               IEnumerable<IUserValidator<User>> userValidators,
                               IEnumerable<IPasswordValidator<User>> passwordValidators,
                               ILookupNormalizer keyNormalizer,
                               IdentityErrorDescriber errors,
                               IServiceProvider services,
                               ILogger<UserManager<User>> logger) : base(store,
                                                                         optionsAccessor,
                                                                         passwordHasher,
                                                                         userValidators,
                                                                         passwordValidators,
                                                                         keyNormalizer,
                                                                         errors,
                                                                         services,
                                                                         logger)
        {
        }
    }

    public class UserControl<TUser> : UserControl<TUser, Guid> where TUser : User
    {
        public UserControl(
            IUserStore<TUser> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<TUser> passwordHasher,
            IEnumerable<IUserValidator<TUser>> userValidators,
            IEnumerable<IPasswordValidator<TUser>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager<TUser>> logger) : base(store,
                                                                 optionsAccessor,
                                                                 passwordHasher,
                                                                 userValidators,
                                                                 passwordValidators,
                                                                 keyNormalizer,
                                                                 errors,
                                                                 services,
                                                                 logger)
        {

        }
    }

    public class UserControl<TUser, TKey> : UserManager<TUser> 
        where TUser : User<TKey>
        where TKey : IEquatable<TKey>
    {
        public UserControl(
            IUserStore<TUser> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<TUser> passwordHasher,
            IEnumerable<IUserValidator<TUser>> userValidators,
            IEnumerable<IPasswordValidator<TUser>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager<TUser>> logger) : base(store,
                                                                 optionsAccessor,
                                                                 passwordHasher,
                                                                 userValidators,
                                                                 passwordValidators,
                                                                 keyNormalizer,
                                                                 errors,
                                                                 services,
                                                                 logger)
        {

        }
    }
}
