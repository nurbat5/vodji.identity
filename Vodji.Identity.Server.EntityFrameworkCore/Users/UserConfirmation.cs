﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserConfirmation : UserConfirmation<User>
    {
    }

    public class UserConfirmation<TUser> : UserConfirmation<TUser, Guid> where TUser : User
    {

    }

    public class UserConfirmation<TUser, TKey> : DefaultUserConfirmation<TUser> where TUser : User<TKey>
        where TKey : IEquatable<TKey>
    {

    }
}
