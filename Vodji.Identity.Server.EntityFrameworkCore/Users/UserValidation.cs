﻿ using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserValidation : UserValidation<User> 
    { }
    
    public class UserValidation<TUser> : UserValidator<TUser> 
        where TUser : User<Guid>
    {

    }

    public class UserValidation<TUser, TKey> : UserValidator<TUser> 
        where TUser : User<TKey>
        where TKey : IEquatable<TKey>
    {

    }
}
