﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{

    public class User : User<Guid> { }
    public class User<TKey> : IdentityUser<TKey> where TKey : IEquatable<TKey>
    {
        public TKey TenantId { get; set; }
        public DateTime Created { get; set; }
    }
}
