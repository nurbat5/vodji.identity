﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserToken : UserToken<Guid> { }
    public class UserToken<TKey> : IdentityUserToken<TKey> where TKey : IEquatable<TKey>
    {

    }
}
