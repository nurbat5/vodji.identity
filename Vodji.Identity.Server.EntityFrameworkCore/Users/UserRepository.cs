﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Vodji.Identity.Server.EntityFrameworkCore.Roles;

namespace Vodji.Identity.Server.EntityFrameworkCore.Users
{
    public class UserRepository<TContext> : UserRepository<TContext, User>
        where TContext : DbContext
    {
        public UserRepository(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }

    public class UserRepository<TContext, TUser> : UserRepository<TContext, TUser, Guid>
        where TUser : User<Guid>
        where TContext : DbContext
    {
        public UserRepository(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }

    public class UserRepository<TContext, TUser, TKey> : UserStore<TUser, Role<TKey>, TContext, TKey, UserClaim<TKey>, UserRole<TKey>, UserLogin<TKey>, UserToken<TKey>, RoleClaim<TKey>>
        where TUser : User<TKey>
        where TContext : DbContext
        where TKey : IEquatable<TKey>
    {
        public UserRepository(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {

        }
    }
}
