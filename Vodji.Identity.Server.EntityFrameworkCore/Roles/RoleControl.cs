﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Vodji.Identity.Server.EntityFrameworkCore.Roles
{
    public class RoleControl : RoleControl<Role>
    {
        public RoleControl(IRoleStore<Role> store, IEnumerable<IRoleValidator<Role>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<Role>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }

    public class RoleControl<TRole> : RoleControl<TRole, Guid>
        where TRole : Role<Guid>
    {
        public RoleControl(IRoleStore<TRole> store, IEnumerable<IRoleValidator<TRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<TRole>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }

    public class RoleControl<TRole, TKey> : RoleManager<TRole>
        where TRole : Role<TKey>
        where TKey : IEquatable<TKey>
    {
        public RoleControl(IRoleStore<TRole> store, IEnumerable<IRoleValidator<TRole>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<TRole>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }
}
