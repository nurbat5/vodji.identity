﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Vodji.Identity.Server.EntityFrameworkCore.Users;

namespace Vodji.Identity.Server.EntityFrameworkCore.Roles
{
    public class RoleRepository<TContext> : RoleRepository<TContext, Role, Guid> where TContext : DbContext
    {
        public RoleRepository(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {

        }
    }

    public class RoleRepository<TContext, TRole> : RoleRepository<TContext, TRole, Guid>
       where TRole : Role<Guid>
       where TContext : DbContext
    {
        public RoleRepository(TContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }

    public class RoleRepository<TContext, TRole, TKey> : RoleStore<TRole, TContext, TKey, UserRole<TKey>, RoleClaim<TKey>>
        where TRole : Role<TKey>
        where TContext : DbContext
        where TKey : IEquatable<TKey>
    {
        public RoleRepository(TContext context, IdentityErrorDescriber describer = null!) : base(context, describer)
        {

        }
    }
}
