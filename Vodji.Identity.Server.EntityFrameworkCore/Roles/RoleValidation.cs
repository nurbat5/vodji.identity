﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Roles
{
    public class RoleValidation : RoleValidation<Role, Guid>
    { }

    public class RoleValidation<TRole> : RoleValidation<TRole, Guid> 
        where TRole : Role<Guid>
    { }

    public class RoleValidation<TRole, TKey> : RoleValidator<TRole> 
        where TRole : Role<TKey>
        where TKey : IEquatable<TKey>
    {
    }
}
