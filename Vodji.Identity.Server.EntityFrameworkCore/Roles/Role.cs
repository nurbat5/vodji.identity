﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Roles
{
    public class Role : Role<Guid> { }
    public class Role<TKey> : IdentityRole<TKey> where TKey : IEquatable<TKey>
    {
    }
}
