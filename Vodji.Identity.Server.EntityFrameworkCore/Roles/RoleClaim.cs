﻿using Microsoft.AspNetCore.Identity;

namespace Vodji.Identity.Server.EntityFrameworkCore.Roles
{
    public class RoleClaim : RoleClaim<Guid> { }
    public class RoleClaim<TKey> : IdentityRoleClaim<TKey> where TKey : IEquatable<TKey>
    {

    }
}
