﻿using System.Security.Claims;

namespace Vodji.Identity.Server.EntityFrameworkCore.Tenants
{
    public class TenantClaim : TenantClaim<Guid> { }

    public class TenantClaim<TKey>
        where TKey : IEquatable<TKey>
    {
        public virtual int Id { get; set; }
        public virtual TKey TenantId { get; set; }
        public virtual string ClaimType { get; set; }
        public virtual string ClaimValue { get; set; }

        public virtual Claim ToClaim()
        {
            return new Claim(ClaimType, ClaimValue);
        }

        public virtual void InitializeFromClaim(Claim claim)
        {
            ClaimType = claim.Type;
            ClaimValue = claim.Value;
        }
    }
}
