﻿namespace Vodji.Identity.Server.EntityFrameworkCore.Tenants
{
    public class Tenant : Tenant<Guid> { }
    public class Tenant<TKey> where TKey : IEquatable<TKey>
    {
        public TKey Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
