﻿using Microsoft.AspNetCore.Mvc;

namespace Vodji.Identity.Server.Api.v1
{
    [Route("api/v1/[controller]")]
    public class UsersController : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "Ok";
        }
    }
}
