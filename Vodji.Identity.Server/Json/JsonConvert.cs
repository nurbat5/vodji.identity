﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Vodji.Identity.Server.Json
{
    public class JsonConvert
    {
        public static async Task<string> SerializeAsync<T>(T objectSerialize)
        {
            if (objectSerialize is null)
            {
                throw new ArgumentNullException(nameof(objectSerialize));
            }

            using var memoryStream = new MemoryStream();
            {
                await JsonSerializer.SerializeAsync(memoryStream, objectSerialize, objectSerialize.GetType(), GetSerializerOptions());
                memoryStream.Position = 0;
            }

            using var memoryReader = new StreamReader(memoryStream);
            {
                return await memoryReader.ReadToEndAsync();
            }
        }

        public static async Task<T> DeserializeAsync<T>(Stream jsonStream)
        {
            if (jsonStream is null)
            {
                throw new ArgumentNullException(nameof(jsonStream));
            }

            var jsonResult = await JsonSerializer.DeserializeAsync<T>(jsonStream, GetSerializerOptions());
            if (jsonResult is not T returnValue)
            {
                throw new NullReferenceException(nameof(jsonResult));
            }

            return returnValue;
        }

        private static JsonSerializerOptions GetSerializerOptions()
        {
            return new JsonSerializerOptions
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
                PropertyNamingPolicy = new JsonConvertNamingPolicy(),
                WriteIndented = true
            };
        }
    }
}
