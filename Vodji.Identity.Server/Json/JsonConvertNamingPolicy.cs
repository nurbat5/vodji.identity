﻿using System.Text.Json;

namespace Vodji.Identity.Server.Json
{
    public class JsonConvertNamingPolicy : JsonNamingPolicy
    {
        public override string ConvertName(string name)
        {
            var nowName = string.Empty;
            for (int index = 0; index < name.Length; index++)
            {
                var letter = name[index];
                if(char.IsUpper(letter))
                {
                    nowName += (index > 0) switch
                    {
                        true => $"_{char.ToLower(letter)}",
                        _ => char.ToLower(letter)
                    };

                    continue;
                }

                nowName += letter;
            }

            return nowName;
        }
    }
}
