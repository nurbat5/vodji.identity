﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Vodji.Identity.Server.OAuth2;

namespace Vodji.Identity.Server.Services.Interfaces
{
    public interface IJwtTokenService
    {
        public Task<JwtSecurityToken?> GetJwtSecurityTokenAsync(string jwtToken, int clockSkew);
        public JwtToken CreateJwtToken(Claim[] claims, int tokenExpires);
    }
}
