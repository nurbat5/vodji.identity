﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Vodji.Identity.Server.Configures;
using Vodji.Identity.Server.OAuth2;
using Vodji.Identity.Server.Services.Interfaces;

namespace Vodji.Identity.Server.Services
{
    public class JwtTokenService : IJwtTokenService
    {
        public JwtTokenService(IOptions<JwtTokenOptions> jwtTokenOptions)
        {
            _jwtTokenOptions = jwtTokenOptions.Value;
            _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        }

        public async Task<JwtSecurityToken?> GetJwtSecurityTokenAsync(string jwtToken, int clockSkew)
        {
            var tokenValidationResult = await _jwtSecurityTokenHandler.ValidateTokenAsync(jwtToken, GetTokenValidationParameters(clockSkew));
            if(tokenValidationResult.IsValid)
            {
                return tokenValidationResult.SecurityToken as JwtSecurityToken;
            }

            return null;
        }

        public JwtToken CreateJwtToken(Claim[] claims, int tokenExpires)
        {
            var claimsLength = claims.Length;
            {
                Array.Resize(ref claims, claimsLength + 3);
                claims[claimsLength++] = new Claim(JwtRegisteredClaimNames.Sub, _jwtTokenOptions.Subject);
                claims[claimsLength++] = new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString());
                claims[claimsLength++] = new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString());
            }

            var symmetricSecurityKey = GetSymmetricSecurityKey();
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _jwtTokenOptions.Issuer,
                audience: _jwtTokenOptions.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddSeconds(tokenExpires),
                signingCredentials: signingCredentials);

            return new JwtToken(jwtSecurityToken);
        }

        private SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            var keyBytes = Encoding.UTF8.GetBytes(_jwtTokenOptions.Key);
            return new SymmetricSecurityKey(keyBytes);
        }
        private TokenValidationParameters GetTokenValidationParameters(int clockSkew)
        {
            return new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                ValidIssuer = _jwtTokenOptions.Issuer,
                ValidAudience = _jwtTokenOptions.Audience,
                IssuerSigningKey = GetSymmetricSecurityKey(),
                ClockSkew = TimeSpan.FromSeconds(clockSkew)
            };
        }

        private readonly JwtTokenOptions _jwtTokenOptions;
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
    }
}
