﻿using Vodji.Identity.Server.Responses;

namespace Vodji.Identity.Server.Auth.OAuth2.v2.Responses
{
    public class TokenResponse : StatusResponse
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }
        public string TokenType { get; set; }
    }
}
