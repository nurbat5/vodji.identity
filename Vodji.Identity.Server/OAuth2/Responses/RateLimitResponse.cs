﻿namespace Vodji.Identity.Server.OAuth2.Responses
{
    public class RateLimitResponse
    {
        public int Limit { get; set; }
        public int Remaining { get; set; }
        public int Reset { get; set; }

    }
}
