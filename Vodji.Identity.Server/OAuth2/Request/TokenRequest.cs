﻿namespace Vodji.Identity.Server.Auth.OAuth2.v2.Request
{
    public class TokenRequest
    {
        public TokenRequest()
        {
            GrantType = string.Empty;
        }

        public string? Username { get; set; }
        public string? Password { get; set; }
        public string GrantType { get; set; }
        public string? ClientSecret { get; set; }
    }
}
