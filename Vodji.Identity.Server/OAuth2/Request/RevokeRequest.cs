﻿namespace Vodji.Identity.Server.Auth.OAuth2.v2.Request
{
    public class RevokeRequest
    {
        public string AccessToken { get; set; }
    }
}
