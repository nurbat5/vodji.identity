﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IdentityModel.Tokens.Jwt;

namespace Vodji.Identity.Server.OAuth2
{
    public class JwtToken
    {
        public string AccessToken { get; }
        public string TokenType { get; }

        public JwtToken(JwtSecurityToken jwtSecurityToken)
        {
            _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            {
                AccessToken = _jwtSecurityTokenHandler.WriteToken(jwtSecurityToken);
                TokenType = JwtBearerDefaults.AuthenticationScheme;
            }
        }

        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
    }
}
