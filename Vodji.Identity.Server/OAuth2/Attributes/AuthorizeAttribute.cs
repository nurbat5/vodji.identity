﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace Vodji.Identity.Server.OAuth2.Attributes
{
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            
        }
    }
}
