﻿using Microsoft.AspNetCore.Mvc;
using Vodji.Identity.Server.Auth.OAuth2.v2.Request;
using Vodji.Identity.Server.OAuth2.Attributes;
using Vodji.Identity.Server.Services.Interfaces;

namespace Vodji.Identity.Server.OAuth2.Controllers
{
    [ApiController]
    [Route("auth/oauth2/v2/[controller]")]
    public class RevokeController : ControllerBase
    {
        public RevokeController(IJwtTokenService jwtTokenService)
        {
            _jwtTokenService = jwtTokenService;
        }

        [HttpPost]
        [Authorize]
        public Task PostRevoke(RevokeRequest revokeRequest)
        {
            //if (revokeRequest is null)
            //{
            //    return HttpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "A non-empty request body is required.");
            //}

            //if (string.IsNullOrWhiteSpace(revokeRequest.AccessToken))
            //{
            //    return HttpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "Access Token Missing.");
            //}

            return HttpContext.Response.WriteAsync("Ok");
            //var successResponse = new StatusResponse(HttpStatusCode.OK, "Success");
            //return await _jsonConvert.SerializeAsync(successResponse);
        }

        private readonly IJwtTokenService _jwtTokenService;
    }
}
