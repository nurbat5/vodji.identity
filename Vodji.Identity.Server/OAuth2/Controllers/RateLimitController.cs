﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Vodji.Identity.Server.Auth.OAuth2.v2.Request;
using Vodji.Identity.Server.Extensions;
using Vodji.Identity.Server.OAuth2.Attributes;
using Vodji.Identity.Server.OAuth2.Responses;
using Vodji.Identity.Server.Services.Interfaces;

namespace Vodji.Identity.Server.OAuth2.Controllers
{
    [ApiController]
    [Route("auth/oauth2/v2/[controller]")]
    public class RateLimitController : ControllerBase
    {
        public RateLimitController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        [HttpPost]
        [Authorize]
        public Task GetRateLimit()
        {
            var jti = HttpContext.User.Claims.First(e => e.Type == "jti");

            var rateLimitResponse = _memoryCache.GetOrCreate(jti.Value, e =>
            {
                e.SetSlidingExpiration(TimeSpan.FromSeconds(3));
                return new RateLimitResponse
                {
                    Limit = 5000,
                    Remaining = 5000,
                    Reset = 3600
                };
            });

            rateLimitResponse.Remaining--;

            var opt = new MemoryCacheEntryOptions
            {
                SlidingExpiration = TimeSpan.FromSeconds(3),
            };

            _memoryCache.Set(jti.Value, rateLimitResponse, opt);
            return HttpContext.Response.WriteAsJsonAsync(rateLimitResponse);
        }

        private readonly IMemoryCache _memoryCache;
    }
}
