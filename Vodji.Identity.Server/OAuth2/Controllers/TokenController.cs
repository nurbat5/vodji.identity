﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Vodji.Identity.Server.Auth.OAuth2.v2.Enums;
using Vodji.Identity.Server.Auth.OAuth2.v2.Request;
using Vodji.Identity.Server.Auth.OAuth2.v2.Responses;
using Vodji.Identity.Server.Extensions;
using Vodji.Identity.Server.Services.Interfaces;

namespace Vodji.Identity.Server.OAuth2.Controllers
{
    [ApiController]
    [Route("auth/oauth2/v2/[controller]")]
    public class TokenController : ControllerBase
    {
        public TokenController(IJwtTokenService jwtTokenService)
        {
            _jwtTokenService = jwtTokenService;
        }

        [HttpPost]
        public Task PostToken(TokenRequest tokenRequest)
        {
            if (HttpContext.Request.ContentType != "application/json")
            {
                return HttpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "Content Type is not specified or specified incorrectly. Content-Type header must be set to application/json.");
            }

            if (tokenRequest is null)
            {
                return HttpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "A non-empty request body is required.");
            }

            var grantTypeNames = Enum.GetNames(typeof(GrantTypeEnum));
            if (!grantTypeNames.Any(e => e.Equals(tokenRequest.GrantType, StringComparison.OrdinalIgnoreCase)))
            {
                return HttpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "grant_type is incorrect/absent");
            }

            var claims = new Claim[]
            {
                //new Claim("UserId", user.UserId.ToString()),
                //new Claim("DisplayName", user.DisplayName),
                //new Claim("UserName", user.UserName),
                //new Claim("Email", user.Email)
            };

            var jwtToken = _jwtTokenService.CreateJwtToken(claims, 3600);
            var tokenResponse = new TokenResponse
            {
                TokenType = jwtToken.TokenType,
                ExpiresIn = 3600,
                AccessToken = jwtToken.AccessToken
            };

            return HttpContext.Response.WriteAsJsonAsync(tokenResponse);
        }

        private readonly IJwtTokenService _jwtTokenService;
    }
}
