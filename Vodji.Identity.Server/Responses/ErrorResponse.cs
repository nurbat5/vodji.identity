﻿namespace Vodji.Identity.Server.Responses
{
    public class ErrorResponse
    {
        public int Code { get; set; }
        public bool Error { get; set; }
        public string Type { get; set; } = string.Empty;
        public string? Message { get; set; }
    }
}
