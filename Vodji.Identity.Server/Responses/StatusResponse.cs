﻿namespace Vodji.Identity.Server.Responses
{
    public class StatusResponse
    {
        public ErrorResponse? Status { get; set; }
    }
}
