﻿using System.Net;
using Vodji.Identity.Server.Extensions;

namespace Vodji.Identity.Server.Middlewares
{
    public class HttpContextResponseMiddleware
    {
        public HttpContextResponseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            await _next(httpContext);

            if (!httpContext.Response.HasStarted && httpContext.Response.StatusCode == 404)
            {
                await httpContext.Response.WriteStatusAsync(HttpStatusCode.NotFound, "No Route Exists");
            }
        }

        private readonly RequestDelegate _next;
    }
}
