﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Security.Claims;
using Vodji.Identity.Server.Extensions;
using Vodji.Identity.Server.Services.Interfaces;

namespace Vodji.Identity.Server.Middlewares
{
    public class JwtBearerMiddleware
    {
        public JwtBearerMiddleware(RequestDelegate next, IJwtTokenService jwtTokenService)
        {
            _next = next;
            _jwtTokenService = jwtTokenService;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var endpoint = httpContext.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAuthorizationFilter>() is null)
            {
                _next(httpContext).GetAwaiter();
                return;
            }

            if (!httpContext.Request.Headers.TryGetValue("Authorization", out var authorizationArray))
            {
                await httpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "The authorization information is missing.");
                return;
            }

            var authorization = authorizationArray.FirstOrDefault()?.Split(' ');
            if (authorization == null || authorization?.Length != 2)
            {
                await httpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, "The authorization information is missing.");
                return;
            }

            var tokenType = authorization[0];
            if (!tokenType.Equals(JwtBearerDefaults.AuthenticationScheme))
            {
                await httpContext.Response.WriteStatusAsync(HttpStatusCode.Unauthorized, "Authentication Failure.");
                return;
            }

            var accessToken = authorization[1];
            var jwtSecurityToken = await _jwtTokenService.GetJwtSecurityTokenAsync(accessToken, 60);
            if (jwtSecurityToken is null)
            {
                await httpContext.Response.WriteStatusAsync(HttpStatusCode.Unauthorized, "Authentication Failure.");
                return;
            }

            var claimsIdentity = new ClaimsIdentity(jwtSecurityToken.Claims, JwtBearerDefaults.AuthenticationScheme);
            {
                httpContext.User = new ClaimsPrincipal();
                httpContext.User.AddIdentity(claimsIdentity);
            }

            _next(httpContext).GetAwaiter();
        }

        private readonly RequestDelegate _next;
        private readonly IJwtTokenService _jwtTokenService;
    }
}
