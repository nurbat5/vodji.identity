﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using Vodji.Identity.Server.Extensions;

namespace Vodji.Identity.Server.Filters
{
    public class ValidationFilter : ActionFilterAttribute
    {
        public override Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (context.Result is BadRequestObjectResult badRequest && badRequest.Value is ValidationProblemDetails details)
            {
                return context.HttpContext.Response.WriteStatusAsync(HttpStatusCode.BadRequest, details.Errors.FirstOrDefault().Value.FirstOrDefault());
            }

            return base.OnResultExecutionAsync(context, next);
        }
    }
}
