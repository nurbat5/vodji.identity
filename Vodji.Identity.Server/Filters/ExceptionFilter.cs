﻿using Microsoft.AspNetCore.Mvc.Filters;
using Vodji.Identity.Server.Extensions;

namespace Vodji.Identity.Server.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override Task OnExceptionAsync(ExceptionContext context)
        {
            return context.HttpContext.Response.WriteStatusAsync(HttpStatusCode.InternalServerError, context.Exception.Message);
        }
    }
}
