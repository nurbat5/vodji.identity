using Microsoft.Extensions.Caching.Memory;
using System.Text.Json.Serialization;
using Vodji.Identity.Server.Extensions;
using Vodji.Identity.Server.Filters;
using Vodji.Identity.Server.Json;
using Vodji.Identity.Server.Middlewares;

var builder = WebApplication.CreateBuilder(args);
{
    var configuration = builder.Configuration;
    var services = builder.Services;
    {
        //services.AddIdentityCoreDefault(default);

        services.AddAuthenticationDefault(configuration);
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        services.AddSingleton<IMemoryCache, MemoryCache>();

        services.AddControllers(options =>
        {
            options.Filters.Add(new ValidationFilter());
            options.Filters.Add(new ExceptionFilter());

            options.AllowEmptyInputInBodyModelBinding = true;
        }).AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.PropertyNamingPolicy = new JsonConvertNamingPolicy();
            options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
            options.JsonSerializerOptions.WriteIndented = true;
        });
    }
}

var app = builder.Build();
{
    app.UseMiddleware<HttpContextResponseMiddleware>();
    app.UseMiddleware<JwtBearerMiddleware>();

    app.UseAuthorization();
    app.MapControllers();
    app.Run();
}
