﻿using System.Text;
using Vodji.Identity.Server.Json;

namespace Vodji.Identity.Server.Extensions
{
    public static class JsonConvertExtension
    {
        public static async Task<T> DeserializeAsync<T>(this JsonConvert jsonConvert, string jsonData)
        {

            if (string.IsNullOrWhiteSpace(jsonData))
            {
                throw new ArgumentNullException(nameof(jsonData));
            }

            using var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(jsonData));
            {
                return await JsonConvert.DeserializeAsync<T>(memoryStream);
            }
        }
    }
}
