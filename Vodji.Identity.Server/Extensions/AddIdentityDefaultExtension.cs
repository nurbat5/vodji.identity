﻿using Microsoft.AspNetCore.Identity;
using Vodji.Identity.Server.EntityFrameworkCore.Context;
using Vodji.Identity.Server.EntityFrameworkCore.Users;

namespace Vodji.Identity.Server.Extensions
{
    public static class AddIdentityCoreDefaultExtension
    {
        public static IdentityBuilder AddIdentityCoreDefault(this IServiceCollection services, Action<IdentityOptions>? setupAction)
        {
            services.AddDbContext<DefaultDbContext>();
            return setupAction is null ? services.AddIdentityCore<User>() : services.AddIdentityCore<User>(setupAction);
        }
    }
}
