﻿using Vodji.Identity.Server.Json;
using Vodji.Identity.Server.Responses;

namespace Vodji.Identity.Server.Extensions
{
    public static class HttpResponseExtension
    {
        public static async Task WriteStatusAsync(this HttpResponse httpResponse, HttpStatusCode statusCode, string message, bool error = true)
        {
            var statusResponse = new StatusResponse
            {
                Status = new ErrorResponse
                {
                    Code = (int)statusCode,
                    Type = statusCode.ToString(),
                    Message = message,
                    Error = error
                }
            };

            httpResponse.StatusCode = (int)statusCode;
            await httpResponse.WriteAsync(await JsonConvert.SerializeAsync(statusResponse));
        }

        public static async Task WriteAsJsonAsync<T>(this HttpResponse httpResponse, T jsonObject)
        {
            httpResponse.ContentType = "application/json";
            var jsonData = await JsonConvert.SerializeAsync(jsonObject);
            await httpResponse.WriteAsync(jsonData);
        }
    }
}
