﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;

namespace Vodji.Identity.Client.RazorComponents.Areas.Auth;

public class SignInComponent : ComponentBase
{
    protected string GetErrorMessage(string errorMessage)
    {
        if(string.IsNullOrWhiteSpace(errorMessage))
        {
            return $"<label class=\"text text-error\">{errorMessage}/label>";
        }

        return default!;
    }

    protected string _passwordErrorMessage = string.Empty; // Email address is required
    protected string _emailErrorMessage = string.Empty; // Email address is required
}